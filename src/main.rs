
struct LinkedList {
    // Option because the linked list starts empty, so head will be null
    head: Option<Box<Node>>,
    size: usize,
}

struct Node {
    val: u32,
    next: Option<Box<Node>>,
}

impl Node {
    pub fn new(val: u32, next: Option<Box<Node>>) -> Node {
       Node {val: val,
            next: next
        } 
    }
}

impl LinkedList {
    pub fn new() -> LinkedList {
        LinkedList {
            head: None,
            size: 0
        }
    }

    pub fn get_size(&self) -> usize {
        self.size
    }

    pub fn is_empty(&self) -> bool {
        self.size == 0    
    }

    pub fn push(&mut self, val: u32) {
        // create a new node
        // self.head.take() gets the value out of head so we can point to it with our new node
        let node: Box<Node> = Box::new(Node::new(val, self.head.take()));
        self.head = Some(node);
        self.size += 1;
    }

    pub fn pop(&mut self) -> Option<u32>{
        // ? will return Some if there's something, otherwise it'll return None
        let node: Box<Node> = self.head.take()?;
        // remove the item from the linked list
        self.head = node.next;
        self.size -= 1;
        Some(node.val)
        // None
    }
}

fn main() {
    let mut list: LinkedList = LinkedList::new();
    for i in 1..10 {
        list.push(i);
    }
    println!("list size: {}", list.get_size());
    println!("Top element: {}", list.pop().unwrap());
    println!("list size: {}", list.get_size());
}
